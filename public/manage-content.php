<?php include '../includes/db-connection.php'; ?>
<?php require '../includes/function.php'; ?>


<?php include '../includes/layouts/header.php'; ?>
<?php
// going to identify what is selected through the navigation


$current_page = null;
$current_subject = null;



if(isset($_GET['subject'])){
	
    $current_subject = get_subject_by_id($_GET['subject']);

}
else if(isset($_GET['page'])){
	
    $current_page = get_page_by_id( $_GET['page']);
}



?>

<!-- Sidebar  navigation-->
<div id="sidebar-wrapper">
	<?php echo navigation($current_subject,$current_page); ?>
</div>
<!-- /#sidebar-wrapper -->

<!-- Page Content -->
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
            	<a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
               
                <?php 
                if( $current_page ){
                   
                        ?> 
                        <h1>Manage Page </h1>
                        <h2> <?php echo $current_page['menu_name'] ?>  </h2>
                        <div> <?php echo ($current_page['content']); ?>  </div> 
                        <?php
                   
                }
              
                    
                else if($current_subject){
                    ?> 
                    <h1>Manage Subject </h1>
                    <h2> <?php echo $current_subject['menu_name'] ?>  </h2>
                    <?php
                }
                
                else {
                    ?>
                    <h2> Please select something to edit !!! </h2>
                      <?php 
                }


                ?>
            </div>
        </div>
    </div>
</div>
<!-- /#page-content-wrapper -->

<?php include '../includes/layouts/footer.php'; ?>