<?php include '../includes/layouts/header.php'; ?>
<!-- Sidebar -->
<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a href="#">
                Start Bootstrap
            </a>
        </li>
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li>
            <a href="#">Shortcuts</a>
        </li>
        <li>
            <a href="#">Overview</a>
        </li>
        <li>
            <a href="#">Events</a>
        </li>
        <li>
            <a href="#">About</a>
        </li>
        <li>
            <a href="#">Services</a>
        </li>
        <li>
            <a href="#">Contact</a>
        </li>
    </ul>
</div>
<!-- /#sidebar-wrapper -->

<!-- Page Content -->
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
                <h1>Welcome to Our Website</h1>
                <li> <a href="manage-content.php">manage content</a> </li>
                <li> <a href="manage-admin.php">manage admin</a> </li>
                <li> <a href="logout.php">logout</a> </li>
            </div>
        </div>
    </div>
</div>
<!-- /#page-content-wrapper -->
<?php include '../includes/layouts/footer.php'; ?>