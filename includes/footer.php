
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="javascripts/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="javascripts/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

</body>
</html>

<?php
   //  Close connection data
    if(isset($connection)){
        mysqli_close($connection);
    }
 ?>