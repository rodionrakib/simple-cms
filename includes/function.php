<?php


function confirm_query($result)
{
	if(!$result){
		die('Database connection failed');
	}

}


function get_all_visible_subjects()
{
	global $connection;
	$query = "SELECT * ";
	$query .= "FROM subjects ";
	$query .= "WHERE visible = 1 ";
	$query .= "ORDER BY position ASC ";


	$subjects = mysqli_query($connection,$query);

	confirm_query($subjects);

	return $subjects;
}

function get_pages_by_subject_id($id)
{
	global $connection;
	$query = "SELECT * ";
	$query .= "FROM pages ";
	$query .= "WHERE visible = 1 ";
	$query .= "AND subject_id =  {$id} ";
	$query .= "ORDER BY position ASC ";


	$page_set = mysqli_query($connection,$query);

	confirm_query($page_set);

	return $page_set;
}

// retrive subject by given id 


function get_subject_by_id($id)
{
	global $connection;
	$safe_id = mysqli_real_escape_string($connection,$id);
	$query = "SELECT * ";
	$query .= "FROM subjects ";
	//$query .= "WHERE visible = 1 ";
	$query .= "WHERE id =  {$safe_id} ";
	$query .= "LIMIT 1 ";


	$selected_subject = mysqli_query($connection,$query);
	confirm_query($selected_subject);
	if($selected_subject = mysqli_fetch_assoc($selected_subject)){
		return $selected_subject;
	}
	else{
		return null;
	}

}

function get_page_by_id($id)
{
	global $connection;
	$safe_id = mysqli_real_escape_string($connection,$id);
	$query = "SELECT * ";
	$query .= "FROM pages ";
	//$query .= "WHERE visible = 1 ";
	$query .= "WHERE id =  {$safe_id} ";
	$query .= "LIMIT 1 ";


	$selected_page = mysqli_query($connection,$query);
	confirm_query($selected_page);
	if($selected_page = mysqli_fetch_assoc($selected_page)){
		return $selected_page;
	}
	else{
		return null;
	}
}


// get either array or null 
function navigation($subject_array,$page_array)
{
    $output =  "<ul class=\"sidebar-nav subjects\">";
	  
		// Perform database query 

		$subjects = get_all_visible_subjects();
		 
		while($subject = mysqli_fetch_assoc($subjects)){
		
			
			$output .= "<li ";

			if( $subject_array &&  ($subject['id']  ===  $subject_array['id']) ) 
			{
			   $output .=  " class= \"selected\" "; 
			} 
			$output .= " >";

			
			$output .="<a href=\"manage-content.php?subject=";


			$output .= urlencode($subject['id']) ;

			$output .= "\">"; 
			
			$output .= $subject['menu_name'] ;
			$output .= "</a>";
			$output .=	"<ul class=\"pages\">";
				

				// find pages belongs to current subject

				$page_set = get_pages_by_subject_id($subject['id']);


				while($page = mysqli_fetch_assoc($page_set)){
				
				$output .= "<li "; 

				 if( $page_array  &&  ($page['id']  ===  $page_array['id']) )
				   { $output .= "class= \"selected\" "; } 
				$output .= ">"; 
				$output .=	"<a href=\"manage-content.php?page=";
				$output .= urlencode($page['id']);
				$output .=	 "\">" ;

				$output .= substr($page['menu_name'],0,20); 

				$output .= "..." ;

				$output .=  "</a>";
				$output .= "</li>";

			   
				}	
				$output .= "</ul>";
			$output .= "</li>";
		}
	
	$output .= "</ul>" ;

	return $output;
}
